#include <stdio.h>
#include <stdlib.h>

typedef struct nodo {
	int numero;
	struct nodo* siguiente;
} nodo;

nodo *cabecera = NULL;
nodo *actual = NULL;

void verLista()
{
	nodo *ptr;
	ptr = cabecera;

	printf("\n[Cabeza] =>");

	while(ptr != NULL){
		printf(" %d =>",ptr->numero);
		ptr = ptr->siguiente;
	}

	printf(" [null]\n");
}


int main (int argc, char *argv[])
{
	nodo *enlace, *temp;
	int num;

	while (1){
		printf("Ingrese un numero o escriba 0 para salir: ");
		scanf("%d",&num);
		// Al agregar la siguiente instrucción se asegura que la
		// primera vez que funcione el bucle 'temp' y 'enlace'
		// apunten a null, pues en la primera vez 'cabecera'
		// apunta a null. Cuando se crea el primer nodo 'enlace'
		// se asegura que 'enlace->siguiente' apunte a null por
		// medio de 'temp'. Al final 'cabecera' apunta a 'enlace'
		// esto es debido a que solo existe un nodo.
		// En las siguientes repeticiones del bucle, 'temp'
		// apunta a 'cabecera' y 'enlace' apunta a 'temp', y
		// cuando el nuevo nodo 'enlace' es creado su atributo
		// 'enlace->siguiente' apunta a 'temp' y asegura que el
		// nodo siguiente al nuevo es el que se considera
		// actualmente la 'cabecera'.
		// Lo que significa que cada vez que el bucle se repita
		// la 'cabecera' anterior va a ser apuntada por
		// 'enlace->siguiente' y la 'cabecera' actual va a
		// apuntar al nodo que ha sido creado.
		// El error se originaba que en el código original
		// ni 'temp' ni 'enlace' apuntaban a null para la primera
		// vez que se ejecutaba el bucle, ocasionando que
		// 'enlace->siguiente' apunte a algo desconocido.
		// y como en las siguientes repeticiones
		// 'enlace' se igualaba nuevamente a 'temp' y este
		// seguía con un valor desconocido o basura,
		// ocasionaba que no se enlace adecuadamente la lista.
		temp = cabecera;
		enlace = temp;
		
		if (num == 0)
			break;
		else{
			enlace = (nodo *) malloc(sizeof(nodo));
			enlace->numero = num;
			enlace->siguiente = temp;
			cabecera = enlace;
		}
	}
	verLista();
}
