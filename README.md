# Práctica 7: Uso de GDB #

Este es un repositorio esqueleto para la práctica 7 de la materia Programación de Sistemas (CCPG1008) P1 de la ESPOL.

### ¿Cómo empiezo? ###

* Hacer un fork de este repositorio a su cuenta personal de Bitbucket (una cuenta por grupo)
* Clonar el repositorio en cuenta (no este) en su computadora del laboratorio
* Completar la práctica en grupo
* Haga commit y push a su trabajo
* El entregable es un enlace al repositorio

### Integrantes ###

Edite esta lista y añada los nombres de los integrantes, luego borre esta línea.

* Luigi Basantes Zambrano
* Carlos Andrés Pogo Ubilla

***

### Observacion ###

> _Al hacer el commit en el laboratorio se guardaron los datos del user global_
> _por esa razón aparece el nombre de: ~~John Jairo Cedeño Cuenca~~,_
> _pero el no intervino en el trabajo._
